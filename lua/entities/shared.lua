if SERVER then
	AddCSLuaFile()

	resource.AddFile("materials/vgui/ttt/icon_jack.vmt")
	resource.AddFile("materials/vgui/ttt/sprite_jack.vmt")
end

local roleName = "JAN"

-- creates global var "ROLE_JAN" and other required things
-- TEAM_[name], data: e.g. icon, color, ...
InitCustomTeam(roleName, {
		icon = "vgui/ttt/sprite_jack",
		color = Color(59, 215, 222, 255)
})

-- important to add roles with this function,
-- because it does more than just access the array ! e.g. updating other arrays
InitCustomRole(roleName, 
	{ -- first param is access for ROLES array => ROLES["JAN"] or ROLES["JESTER"]
		color = Color(59, 215, 222, 255), -- ...
		dkcolor = Color(1, 184, 193, 255), -- ...
		bgcolor = Color(255, 154, 67, 255), -- ...
		abbr = "jan", -- abbreviation
		defaultTeam = ROLE_JAN, -- the team name: roles with same team name are working together
		defaultEquipment = INNO_EQUIPMENT, -- here you can set up your own default equipment
		surviveBonus = 0, -- bonus multiplier for every survive while another player was killed
		scoreKillsMultiplier = 0, -- multiplier for kill of player of another team
		scoreTeamKillsMultiplier = 0, -- multiplier for teamkill
		traitorCreditAward = false, -- will receive credits on kill like a traitor
		notSelectable = true
	},	{
		
	}
)


hook.Add("TTT2FinishedLoading", "JanInitT", function()

	if CLIENT then
		-- setup here is not necessary but if you want to access the role data, you need to start here
		-- setup basic translation !
		LANG.AddToLanguage("English", JAN.name, "Jan")
		LANG.AddToLanguage("English", ROLE_JAN, "ROLE JAN")
		LANG.AddToLanguage("English", "hilite_win_" .. ROLE_JAN, "THE JAN WON") -- name of base role of a team -> maybe access with GetBaseRole(ROLE_JESTER) or JESTER.baserole
		LANG.AddToLanguage("English", "win_" .. ROLE_JAN, "The JAN has won!") -- teamname
		LANG.AddToLanguage("English", "info_popup_" .. JAN.name, [[You are the JAN! Kill the Penstealer if he stole the pen!]])
		LANG.AddToLanguage("English", "body_found_" .. JAN.abbr, "This was a Jan...")
		LANG.AddToLanguage("English", "search_role_" .. JAN.abbr, "This person was a Jan!")
		LANG.AddToLanguage("English", "ev_win_" .. ROLE_JAN, "The Jan won the round!")
		LANG.AddToLanguage("English", "target_" .. JAN.name, "Jan")
		LANG.AddToLanguage("English", "ttt2_desc_" .. JAN.name, [[The Jan is shown as Innocent. His goal is to kill the Penstealer in time if he stole the pen! Good Luck!]])

		---------------------------------

		-- maybe this language as well...
		LANG.AddToLanguage("Deutsch", JAN.name, "Jan")
		LANG.AddToLanguage("Deutsch", ROLE_JAN, "ROLE JAN")
		LANG.AddToLanguage("Deutsch", "hilite_win_" .. ROLE_JAN, "THE JAN WON")
		LANG.AddToLanguage("Deutsch", "win_" .. ROLE_JAN, "Der Jan hat gewonnen!")
		LANG.AddToLanguage("Deutsch", "info_popup_" .. JAN.name, [[Du bist DER JAN! Töte den Stiftklauer, wenn er den Stift geklaut hat!]])
		LANG.AddToLanguage("Deutsch", "body_found_" .. JAN.abbr, "Er war der Jan...")
		LANG.AddToLanguage("Deutsch", "search_role_" .. JAN.abbr, "Diese Person war der Jan!")
		LANG.AddToLanguage("Deutsch", "ev_win_" .. ROLE_JAN, "Der Jan hat die Runde gewonnen!")
		LANG.AddToLanguage("Deutsch", "target_" .. JAN.name, "Jan")
		LANG.AddToLanguage("Deutsch", "ttt2_desc_" .. JAN.name, [[Der Jan wird als Unschuldiger angezeigt. Sein Ziel ist es den Stiftklauer innerhalb des Zeitlimits zu töten, wenn er den Stift geklaut hat! Viel Glück!]])
	end
end)